// console.log("Hello World!");
// S26-27 Activity Intro to Database


{
	"first name": "Harry",
	"last name": "Potter",
	"email": "harrypotter@hogwarts.com",
	"password": "VoldermortNoNose",
	"isAdmin": "false",
	"phone": "1-800-123-4567"	
}

{
	"userId": "harrywizard11",
	"transactionDate": "03-27-2023",
	"status": "pending",
	"total": "1499.99"
}

{
	"productName": "magic wand",
	"productDescription": "elder wand",
	"productPrice": "1499.99",
	"productStocks": "1",
	"isActive": "true",
	"productSKU": "777-777"	
}

{
	"orderID": "03-27-23-ew777",
	"productID": "elderwand-777",
	"quantity": "1",
	"price": "1499.99",
	"subtotal": "1499.99"	
}

